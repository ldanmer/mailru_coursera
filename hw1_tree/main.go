package main

import (
	"bytes"
	"fmt"
	"io"
	"os"
	"path"
	"sort"
)

const startSymbol string = "├───"
const lineSymbol string = "│\t"
const endSymbol string = "└───"

var exclude = []string{"hw1.md", "Dockerfile"}

type Path struct {
	name string
	size string
}

func main() {
	out := os.Stdout
	if !(len(os.Args) == 2 || len(os.Args) == 3) {
		panic("usage go run main.go . [-f]")
	}
	startPoint := os.Args[1]

	printFiles := len(os.Args) == 3 && os.Args[2] == "-f"
	err := dirTree(out, startPoint, printFiles)
	if err != nil {
		panic(err.Error())
	}
}

func dirTree(out io.Writer, path string, printFiles bool) error {
	buffer := tree(path, "", printFiles, new(bytes.Buffer))
	_, err := out.Write(buffer.Bytes())

	if err != nil {
		return err
	}
	return nil
}

func tree(base string, prefix string, printFiles bool, buffer *bytes.Buffer) *bytes.Buffer {

	pathSlice := dirNames(base, printFiles)

	for index, filePath := range pathSlice {
		subPath := path.Join(base, filePath.name)

		buffer.WriteString(prefix)

		if index == len(pathSlice)-1 {
			buffer.WriteString(endSymbol)
			buffer.WriteString(filePath.name)
			buffer.WriteString(filePath.size)
			buffer.WriteString("\n")
			tree(subPath, prefix+"\t", printFiles, buffer)
		} else {
			buffer.WriteString(startSymbol)
			buffer.WriteString(filePath.name)
			buffer.WriteString(filePath.size)
			buffer.WriteString("\n")
			tree(subPath, prefix+lineSymbol, printFiles, buffer)
		}
	}

	return buffer
}

func dirNames(base string, printFiles bool) []Path {
	file, err := os.Open(base)
	if err != nil {
		fmt.Println(err)
	}
	defer file.Close()

	names, _ := file.Readdirnames(0)
	sort.Strings(names)

	pathSlice := make([]Path, 0)

	for _, name := range names {

		if name[0] == '.' || inSlice(exclude, name) {
			continue
		}

		subPath := path.Join(base, name)
		fi, _ := os.Stat(subPath)

		if !printFiles && !fi.IsDir() {
			continue
		}

		var sizeString string
		if !fi.IsDir() {
			size := fi.Size()
			if size > 0 {
				sizeString = fmt.Sprintf(" (%db)", size)
			} else {
				sizeString = " (empty)"
			}
		}

		pathSlice = append(pathSlice, Path{
			name: name,
			size: sizeString,
		})
	}
	return pathSlice
}

func inSlice(hash []string, needle string) bool {
	for _, el := range hash {
		if el == needle {
			return true
		}
	}
	return false
}
