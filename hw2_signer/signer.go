package main

import (
	"sort"
	"strconv"
	"strings"
	"sync"
)

const TH int = 6

// accepts a bunch of Jobs
// and executes it in a pipeline
func ExecutePipeline(freeFlowJobs ...job) {
	wg := &sync.WaitGroup{}
	in := make(chan interface{})
	for _, jobCall := range freeFlowJobs {
		wg.Add(1)
		out := make(chan interface{})
		go func(group *sync.WaitGroup, currentJob job, in, out chan interface{}) {
			defer group.Done()
			currentJob(in, out)
			close(out)
		}(wg, jobCall, in, out)
		in = out
	}
	wg.Wait()
}

// считает значение crc32(data)+"~"+crc32(md5(data))
// (конкатенация двух строк через ~),
// где data - то что пришло на вход (по сути - числа из первой функции)
func SingleHash(in, out chan interface{}) {
	wg := &sync.WaitGroup{}
	for v := range in {
		wg.Add(1)
		value := strconv.Itoa(v.(int))
		signedValue := DataSignerMd5(value)
		chan1 := make(chan string)
		chan2 := make(chan string)

		go func(group *sync.WaitGroup, out chan interface{}) {
			defer group.Done()
			go singleSigner(value, chan1)
			go singleSigner(signedValue, chan2)
			value := <-chan1
			signedValue := <-chan2
			out <- value + "~" + signedValue
		}(wg, out)
	}
	wg.Wait()
}

// считает значение crc32(th+data))
// (конкатенация цифры, приведённой к строке и строки), где th=0..5
// (т.е. 6 хешей на каждое входящее значение),
// потом берёт конкатенацию результатов в порядке расчета (0..5),
// где data - то что пришло на вход (и ушло на выход из SingleHash)
func MultiHash(in, out chan interface{}) {
	wg := &sync.WaitGroup{}

	for v := range in {
		value := v.(string)
		wg.Add(1)
		go multiHashWorker(wg, value, out)
	}
	wg.Wait()
}

// получает все результаты, сортирует,
// объединяет отсортированный результат через _ (символ подчеркивания) в одну строку
func CombineResults(in, out chan interface{}) {
	var resultSlice []string
	for v := range in {
		resultSlice = append(resultSlice, v.(string))
	}
	sort.Strings(resultSlice)
	out <- strings.Join(resultSlice, "_")
}

func singleSigner(value string, ch chan string) {
	ch <- DataSignerCrc32(value)
}

func multiHashWorker(group *sync.WaitGroup, value string, out chan interface{}) {
	defer group.Done()
	wg := &sync.WaitGroup{}
	hashSlice := make([]string, TH)
	for i := 0; i < TH; i++ {
		wg.Add(1)
		go func(wg *sync.WaitGroup, i int, value string, hashSlice []string) {
			defer wg.Done()
			th := strconv.Itoa(i)
			hashSlice[i] = DataSignerCrc32(th + value)
		}(wg, i, value, hashSlice)
	}
	wg.Wait()
	out <- strings.Join(hashSlice, "")
}
