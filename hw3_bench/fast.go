package main

import (
	"bufio"
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/mailru/easyjson"
	"github.com/mailru/easyjson/jlexer"
	"github.com/mailru/easyjson/jwriter"
	"io"
	"os"
	"strconv"
	"strings"
)

func FastSearch(out io.Writer) {
	file, err := os.Open(filePath)
	if err != nil {
		panic(err)
	}
	defer file.Close()

	fileContents := bufio.NewReader(file)

	seenBrowsers := make([]string, 0, 114)
	foundUsers := new(bytes.Buffer)
	foundUsers.WriteString("found users:\n")

	i := 0
	user := &User{}
	for {
		line, _, err := fileContents.ReadLine()

		if err == io.EOF {
			break
		}

		err = user.UnmarshalJSON([]byte(line))

		if err != nil {
			panic(err)
		}

		var isAndroid bool
		var isMSIE bool

		for _, browser := range user.Browsers {
			if ok := strings.Contains(browser, "Android"); ok {
				isAndroid = true
				seenBrowsersAppend(&seenBrowsers, &browser)
			} else if ok := strings.Contains(browser, "MSIE"); ok {
				isMSIE = true
				seenBrowsersAppend(&seenBrowsers, &browser)
			}
		}

		if !(isAndroid && isMSIE) {
			i++
			continue
		}

		email := strings.Replace(user.Email, "@", " [at] ", 1)
		foundUsers.WriteString("[")
		foundUsers.WriteString(strconv.Itoa(i))
		foundUsers.WriteString("] ")
		foundUsers.WriteString(user.Name)
		foundUsers.WriteString(" <")
		foundUsers.WriteString(email)
		foundUsers.WriteString(">\n")

		_, err = out.Write(foundUsers.Bytes())
		if err != nil {
			panic(err)
		}
		foundUsers.Reset()
		i++
	}
	foundUsers.WriteString("\n")
	_, err = out.Write(foundUsers.Bytes())
	if err != nil {
		panic(err)
	}
	_, err = fmt.Fprintln(out, "Total unique browsers", len(seenBrowsers))
}

func seenBrowsersAppend(seenBrowsers *[]string, browser *string) {
	notSeenBefore := true
	for _, item := range *seenBrowsers {
		if item == *browser {
			notSeenBefore = false
		}
	}
	if notSeenBefore {
		*seenBrowsers = append(*seenBrowsers, *browser)
	}
}


//easyjson:json
type User struct {
	Browsers []string
	Email    string
	Name     string
}


var (
	_ *json.RawMessage
	_ *jlexer.Lexer
	_ *jwriter.Writer
	_ easyjson.Marshaler
)

func easyjson3486653aDecodeGolang20191599HwOptimization(in *jlexer.Lexer, out *User) {
	isTopLevel := in.IsStart()
	if in.IsNull() {
		if isTopLevel {
			in.Consumed()
		}
		in.Skip()
		return
	}
	in.Delim('{')
	for !in.IsDelim('}') {
		key := in.UnsafeString()
		in.WantColon()
		if in.IsNull() {
			in.Skip()
			in.WantComma()
			continue
		}
		switch key {
		case "browsers":
			if in.IsNull() {
				in.Skip()
				out.Browsers = nil
			} else {
				in.Delim('[')
				if out.Browsers == nil {
					if !in.IsDelim(']') {
						out.Browsers = make([]string, 0, 4)
					} else {
						out.Browsers = []string{}
					}
				} else {
					out.Browsers = (out.Browsers)[:0]
				}
				for !in.IsDelim(']') {
					var v1 string
					v1 = string(in.String())
					out.Browsers = append(out.Browsers, v1)
					in.WantComma()
				}
				in.Delim(']')
			}
		case "email":
			out.Email = string(in.String())
		case "name":
			out.Name = string(in.String())
		default:
			in.SkipRecursive()
		}
		in.WantComma()
	}
	in.Delim('}')
	if isTopLevel {
		in.Consumed()
	}
}
func easyjson3486653aEncodeGolang20191599HwOptimization(out *jwriter.Writer, in User) {
	out.RawByte('{')
	first := true
	_ = first
	{
		const prefix string = ",\"browsers\":"
		if first {
			first = false
			out.RawString(prefix[1:])
		} else {
			out.RawString(prefix)
		}
		if in.Browsers == nil && (out.Flags&jwriter.NilSliceAsEmpty) == 0 {
			out.RawString("null")
		} else {
			out.RawByte('[')
			for v2, v3 := range in.Browsers {
				if v2 > 0 {
					out.RawByte(',')
				}
				out.String(string(v3))
			}
			out.RawByte(']')
		}
	}
	{
		const prefix string = ",\"email\":"
		if first {
			first = false
			out.RawString(prefix[1:])
		} else {
			out.RawString(prefix)
		}
		out.String(string(in.Email))
	}
	{
		const prefix string = ",\"name\":"
		if first {
			first = false
			out.RawString(prefix[1:])
		} else {
			out.RawString(prefix)
		}
		out.String(string(in.Name))
	}
	out.RawByte('}')
}

// MarshalJSON supports json.Marshaler interface
func (v User) MarshalJSON() ([]byte, error) {
	w := jwriter.Writer{}
	easyjson3486653aEncodeGolang20191599HwOptimization(&w, v)
	return w.Buffer.BuildBytes(), w.Error
}

// MarshalEasyJSON supports easyjson.Marshaler interface
func (v User) MarshalEasyJSON(w *jwriter.Writer) {
	easyjson3486653aEncodeGolang20191599HwOptimization(w, v)
}

// UnmarshalJSON supports json.Unmarshaler interface
func (v *User) UnmarshalJSON(data []byte) error {
	r := jlexer.Lexer{Data: data}
	easyjson3486653aDecodeGolang20191599HwOptimization(&r, v)
	return r.Error()
}

// UnmarshalEasyJSON supports easyjson.Unmarshaler interface
func (v *User) UnmarshalEasyJSON(l *jlexer.Lexer) {
	easyjson3486653aDecodeGolang20191599HwOptimization(l, v)
}

