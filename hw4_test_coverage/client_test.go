package main

import (
	"encoding/json"
	"encoding/xml"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"reflect"
	"sort"
	"strconv"
	"strings"
	"testing"
	"time"
)

func TestRequestParams(t *testing.T) {
	ts := httptest.NewServer(http.HandlerFunc(SearchServer))
	cases := []struct {
		request  SearchRequest
		expected *SearchResponse
		Err      bool
	}{
		{
			request: SearchRequest{
				Limit:      -1,
				Offset:     0,
				Query:      "am",
				OrderField: "name",
				OrderBy:    OrderByDesc,
			},
			Err: true,
		},
		{
			request: SearchRequest{
				Limit:      0,
				Offset:     0,
				Query:      "i",
				OrderField: "name",
				OrderBy:    OrderByAsc,
			},
			Err: true,
		},
		{
			request: SearchRequest{
				Limit:      2,
				Offset:     -1,
				Query:      "i",
				OrderField: "name",
				OrderBy:    OrderByAsc,
			},
			Err: true,
		},
		{
			request: SearchRequest{
				Limit:      2,
				Offset:     0,
				Query:      "anim a",
				OrderField: "wrongField",
				OrderBy:    OrderByDesc,
			},
			Err: true,
		},
		{
			request: SearchRequest{
				Limit:      3,
				Offset:     4,
				Query:      "anim a",
				OrderField: "id",
				OrderBy:    OrderByAsc,
			},
			Err: true,
		},
		{
			request: SearchRequest{
				Limit:      3,
				Offset:     0,
				Query:      "123",
				OrderField: "id",
				OrderBy:    OrderByAsc,
			},
			Err: true,
		},
		{
			expected: &SearchResponse{
				[]User{
					{
						Id:     12,
						Age:    36,
						Name:   "Cruz Guerrero",
						About:  "Sunt enim ad fugiat minim id esse proident laborum magna magna. Velit anim aliqua nulla laborum consequat veniam reprehenderit enim fugiat ipsum mollit nisi. Nisi do reprehenderit aute sint sit culpa id Lorem proident id tempor. Irure ut ipsum sit non quis aliqua in voluptate magna. Ipsum non aliquip quis incididunt incididunt aute sint. Minim dolor in mollit aute duis consectetur.\n",
						Gender: "male",
					},
					{
						Id:     11,
						Age:    32,
						Name:   "Gilmore Guerra",
						About:  "Labore consectetur do sit et mollit non incididunt. Amet aute voluptate enim et sit Lorem elit. Fugiat proident ullamco ullamco sint pariatur deserunt eu nulla consectetur culpa eiusmod. Veniam irure et deserunt consectetur incididunt ad ipsum sint. Consectetur voluptate adipisicing aute fugiat aliquip culpa qui nisi ut ex esse ex. Sint et anim aliqua pariatur.\n",
						Gender: "male",
					},
				},
				true,
			},

			request: SearchRequest{
				Limit:      2,
				Offset:     0,
				Query:      "anim a",
				OrderField: "age",
				OrderBy:    OrderByDesc,
			},
			Err: false,
		},
		{
			expected: &SearchResponse{
				[]User{
					{
						Id:     25,
						Age:    32,
						Name:   "Katheryn Jacobs",
						About:  "Magna excepteur anim amet id consequat tempor dolor sunt id enim ipsum ea est ex. In do ea sint qui in minim mollit anim est et minim dolore velit laborum. Officia commodo duis ut proident laboris fugiat commodo do ex duis consequat exercitation. Ad et excepteur ex ea exercitation id fugiat exercitation amet proident adipisicing laboris id deserunt. Commodo proident laborum elit ex aliqua labore culpa ullamco occaecat voluptate voluptate laboris deserunt magna.\n",
						Gender: "female",
					},
					{
						Id:     12,
						Age:    36,
						Name:   "Cruz Guerrero",
						About:  "Sunt enim ad fugiat minim id esse proident laborum magna magna. Velit anim aliqua nulla laborum consequat veniam reprehenderit enim fugiat ipsum mollit nisi. Nisi do reprehenderit aute sint sit culpa id Lorem proident id tempor. Irure ut ipsum sit non quis aliqua in voluptate magna. Ipsum non aliquip quis incididunt incididunt aute sint. Minim dolor in mollit aute duis consectetur.\n",
						Gender: "male",
					},
					{
						Id:     11,
						Age:    32,
						Name:   "Gilmore Guerra",
						About:  "Labore consectetur do sit et mollit non incididunt. Amet aute voluptate enim et sit Lorem elit. Fugiat proident ullamco ullamco sint pariatur deserunt eu nulla consectetur culpa eiusmod. Veniam irure et deserunt consectetur incididunt ad ipsum sint. Consectetur voluptate adipisicing aute fugiat aliquip culpa qui nisi ut ex esse ex. Sint et anim aliqua pariatur.\n",
						Gender: "male",
					},
				},
				false,
			},

			request: SearchRequest{
				Limit:      30,
				Offset:     0,
				Query:      "anim a",
				OrderField: "id",
				OrderBy:    OrderByDesc,
			},
			Err: false,
		},
	}

	client := SearchClient{
		AccessToken: Token,
		URL:         ts.URL,
	}

	for i, item := range cases {
		resp, err := client.FindUsers(item.request)
		if err != nil && !item.Err {
			t.Errorf("[%d] unexpected error: %#v", i, err)
		}
		if err == nil && item.Err {
			t.Errorf("[%d] expected error, got nil", i)
		}
		if !reflect.DeepEqual(item.expected, resp) {
			t.Errorf("[%d] wrong result, expected %#v\n, got %#v", i, item.expected, resp)
		}
	}
	ts.Close()
}

func TestSearchClient(t *testing.T) {
	ts := httptest.NewServer(http.HandlerFunc(SearchServer))
	cases := []struct {
		Client SearchClient
		Err    bool
	}{
		{
			Client: SearchClient{
				AccessToken: Token,
				URL:         ts.URL,
			},
			Err: false,
		},
		{
			Client: SearchClient{
				AccessToken: Token,
				URL:         "",
			},
			Err: true,
		},
		{
			Client: SearchClient{
				AccessToken: "qwee",
				URL:         ts.URL,
			},
			Err: true,
		},
	}
	req := SearchRequest{
		Limit:      1,
		Offset:     0,
		Query:      "Boyd",
		OrderField: "name",
		OrderBy:    OrderByDesc,
	}
	for i, item := range cases {
		_, err := item.Client.FindUsers(req)
		if err != nil && !item.Err {
			t.Errorf("[%d] Expected error %#v", i, err)
		}
	}
	ts.Close()
}

func TestFindFile(t *testing.T) {
	DataSet = "wrongFile.xml"
	defer func() { DataSet = "dataset.xml" }()
	ts := httptest.NewServer(http.HandlerFunc(SearchServer))
	client := SearchClient{
		AccessToken: Token,
		URL:         ts.URL,
	}
	req := SearchRequest{
		Limit:      5,
		Offset:     0,
		Query:      "a",
		OrderField: "age",
		OrderBy:    OrderByDesc,
	}
	_, err := client.FindUsers(req)
	if err == nil {
		t.Errorf("Exptected error")
	}
	ts.Close()
}

func TestClient(t *testing.T) {
	ts := httptest.NewServer(http.HandlerFunc(SearchServer))
	client.Timeout = time.Nanosecond
	defer func() { client.Timeout = time.Second }()
	req := SearchRequest{
		Limit:      5,
		Offset:     0,
		Query:      "a",
		OrderField: "age",
		OrderBy:    OrderByDesc,
	}
	client := SearchClient{
		AccessToken: Token,
		URL:         ts.URL,
	}
	_, err := client.FindUsers(req)
	if err == nil {
		t.Errorf("Exptected error")
	}
	ts.Close()
}

/* Server */

var DataSet = "dataset.xml"
var Token = "123"

type Record struct {
	Id        uint   `xml:"id" json:"Id"`
	Age       uint8  `xml:"age" json:"Age"`
	FirstName string `xml:"first_name" json:"-"`
	LastName  string `xml:"last_name" json:"-"`
	Gender    string `xml:"gender" json:"Gender"`
	About     string `xml:"about" json:"About"`
	Name      string `json:"Name"`
}

type row struct {
	Records []Record `xml:"row"`
}

func SearchServer(w http.ResponseWriter, r *http.Request) {
	rows := new(*row)
	dataFile, err := ioutil.ReadFile(DataSet)
	if err != nil {
		serverError(w, "Cannot read XML file", err)
		return
	}

	err = xml.Unmarshal(dataFile, rows)
	if err != nil {
		serverError(w, "XML decode error", err)
		return
	}

	AccessToken := r.Header.Get("AccessToken")
	if AccessToken != Token {
		w.WriteHeader(http.StatusUnauthorized)
		return
	}
	query := r.URL.Query().Get("query")
	limit, _ := strconv.Atoi(r.URL.Query().Get("limit"))
	offset, _ := strconv.Atoi(r.URL.Query().Get("offset"))
	orderField := r.URL.Query().Get("order_field")
	orderBy, _ := strconv.Atoi(r.URL.Query().Get("order_by"))

	if limit == 1 {
		badRequest(w, "Limit too small")
		return
	}

	if orderField == "" {
		orderField = "name"
	}

	var result []Record
	if query != "" {
		for _, value := range (*rows).Records {
			value.Name = value.FirstName + " " + value.LastName
			if strings.Contains(value.Name, query) || strings.Contains(value.About, query) {
				result = append(result, value)
			}
		}
	} else {
		result = (*rows).Records
	}

	if len(result) == 0 {
		badRequest(w, "Result of null")
	}

	if orderBy != OrderByAsIs {
		if orderField != "age" && orderField != "id" && orderField != "name" {
			badRequest(w, "ErrorBadOrderField")
			return
		}
		order(result, orderField, orderBy)
	}
	if offset > len(result) {
		fmt.Fprint(w, "Offset if bigger then result")
		return
	}
	result = result[offset:]

	if limit <= len(result) {
		result = result[:limit]
	}

	response, _ := json.Marshal(&result)
	fmt.Fprint(w, string(response))
}

func serverError(w http.ResponseWriter, msg string, err error) {
	w.WriteHeader(http.StatusInternalServerError)
	response, _ := json.Marshal(msg + ": " + err.Error())
	fmt.Fprint(w, string(response))
}

func badRequest(w http.ResponseWriter, msg string) {
	response, _ := json.Marshal(SearchErrorResponse{msg})
	w.WriteHeader(http.StatusBadRequest)
	fmt.Fprint(w, string(response))
}

func order(records []Record, orderField string, orderBy int) {
	switch orderField {
	case "id":
		sort.SliceStable(records, func(i, j int) bool {
			if orderBy == OrderByDesc {
				return records[i].Id > records[j].Id
			}
			return records[i].Id < records[j].Id
		})
	case "name":
		sort.SliceStable(records, func(i, j int) bool {
			if orderBy == OrderByDesc {
				return records[i].Name > records[j].Name
			}
			return records[i].Name < records[j].Name
		})
	case "age":
		sort.SliceStable(records, func(i, j int) bool {
			if orderBy == OrderByDesc {
				return records[i].Age > records[j].Age
			}
			return records[i].Age < records[j].Age
		})
	}
}
