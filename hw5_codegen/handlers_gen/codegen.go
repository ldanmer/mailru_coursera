package main

import (
	"encoding/json"
	"fmt"
	"go/ast"
	"go/parser"
	"go/token"
	"log"
	"os"
	"reflect"
	"regexp"
	"strings"
	"text/template"
)

// код писать тут

const (
	AttrApigen    = "// apigen:api "
	AttrValidator = "apivalidator"
)

var (
	TagRequired  = regexp.MustCompile("required")
	TagParamname = regexp.MustCompile("paramname=(.+)")
	TagEnum      = regexp.MustCompile("enum=(.+)")
	TagMin       = regexp.MustCompile("min=(.+)")
	TagMax       = regexp.MustCompile("max=(.+)")
	TagDefault   = regexp.MustCompile("default=(.+)")
)

type resource struct {
	Url    string `json:"url"`
	Auth   bool   `json:"auth"`
	Method string `json:"method,omitempty"`
	Func   string
	Param  string
}

type tpl struct {
	ResourceName string
	Resources    []resource
}

var tplHandler = template.Must(template.New("tplHandler").Parse(`
func (srv *{{.ResourceName}}) ServeHTTP(rw http.ResponseWriter, r *http.Request) {
switch r.URL.Path {
{{range $i, $a := .Resources}}
case "{{$a.Url}}":
	{{if $a.Method}}
	if r.Method == "{{$a.Method}}" {
	{{end}}
		{{if $a.Auth}}
		if r.Header.Get("X-Auth") != "100500" {
			errorMsg, _ := json.Marshal(map[string]interface{}{"error": "unauthorized"})
			http.Error(rw, string(errorMsg), http.StatusForbidden)
			return
		}
		{{end}}
		params, err := validate{{$a.Param}}(r)
		if err != nil {
			errorMsg, _ := json.Marshal(map[string]interface{}{"error": err.Error()})
			http.Error(rw, string(errorMsg), http.StatusBadRequest)
			return
		}
		responseVal, err := srv.{{$a.Func}}(r.Context(), *params)
		if err != nil {
			if apiError, ok := err.(ApiError); ok {
				errorMsg, _ := json.Marshal(map[string]interface{}{"error": apiError.Error()})
				http.Error(rw, string(errorMsg), apiError.HTTPStatus)
			} else {
				errorMsg, _ := json.Marshal(map[string]interface{}{"error": err.Error()})
				http.Error(rw, string(errorMsg), http.StatusInternalServerError)
			}
			return
		}
		data, err := json.Marshal(map[string]interface{}{"error": "", "response": responseVal})
		if err != nil {
			http.Error(rw, err.Error(), http.StatusInternalServerError)
			return
		}
		rw.Write(data)
	{{if $a.Method}}
	} else {
		errorMsg, _ := json.Marshal(map[string]interface{}{"error": "bad method"})
		http.Error(rw, string(errorMsg), http.StatusNotAcceptable)
	}
	{{end}}
{{end}}
default:
	errorMsg, _ := json.Marshal(map[string]interface{}{"error": "unknown method"})
		http.Error(rw, string(errorMsg), http.StatusNotFound)
	}
}
`))

type tplField struct {
	Name         string
	Param        string
	Default      string
	Required     bool
	IsEnum       bool
	Enums        string
	EnumType     string
	IsDefault    bool
	DefaultValue string
	IsMax        bool
	Max          string
	IsMin        bool
	Min          string
	EnumError    string
}

type paramValidate struct {
	Param  string
	Fields []tplField
}

var tplValidation = template.Must(template.New("tplValidation").Parse(`
func validate{{.Param}}(r *http.Request) (*{{.Param}}, error) {
params := &{{.Param}}{}
var err error
{{range $i, $f := .Fields}}
	params.{{$f.Name}}{{if eq $f.Default "0"}}, err = strconv.Atoi(r.Form.Get("{{$f.Param}}"))
	if err!=nil {
		return nil, fmt.Errorf("{{$f.Param}} must be int")
	}{{else}} = r.FormValue("{{$f.Param}}"){{end}}
	{{if $f.IsDefault}}
	if params.{{$f.Name}} == {{$f.Default}} {
		params.{{$f.Name}} = {{$f.DefaultValue}}
	}
	{{end}}
	{{if $f.Required}}
	if params.{{$f.Name}} == {{$f.Default}} {
		return nil, fmt.Errorf("{{$f.Param}} must me not empty")
	}
	{{end}}
	{{if $f.IsEnum}}
	valid := func(enums []{{$f.EnumType}}) bool{
		for _, enum := range enums{
			if params.{{$f.Name}} == enum {
				return true
			}
		}
		return false
	}([]{{$f.EnumType}}{ {{$f.Enums}} })
	if !valid {
		return nil, fmt.Errorf("{{$f.EnumError}}")
	}
	{{end}}
	{{if $f.IsMax}}
	if {{$f.Max}} <= {{if eq $f.Default "0"}} params.{{$f.Name}} {{else}} len([]rune(params.{{$f.Name}})) {{end}}{
		return nil, fmt.Errorf("{{$f.Param}} {{if eq $f.Default "0"}}{{else}}len {{end}}must be <= {{$f.Max}}")
	}
	{{end}}
	{{if $f.IsMin}}
	if {{$f.Min}} > {{if eq $f.Default "0"}} params.{{$f.Name}} {{else}} len([]rune(params.{{$f.Name}})) {{end}}{
		return nil, fmt.Errorf("{{$f.Param}} {{if eq $f.Default "0"}}{{else}}len {{end}}must be >= {{$f.Min}}")
	}
	{{end}}
{{end}}
return params, err
}
`))

func main() {
	fileSet := token.NewFileSet()
	generator, err := parser.ParseFile(fileSet, os.Args[1], nil, parser.ParseComments)
	if err != nil {
		log.Fatal(err)
	}

	result, err := os.Create(os.Args[2])
	if err != nil {
		log.Fatal(err)
	}

	_, err = fmt.Fprintln(result, "package "+generator.Name.Name)
	_, err = fmt.Fprintln(result)
	_, err = fmt.Fprintln(result, `import "encoding/json"`)
	_, err = fmt.Fprintln(result, `import "fmt"`)
	_, err = fmt.Fprintln(result, `import "net/http"`)
	_, err = fmt.Fprintln(result, `import "strconv"`)

	paramTpls := make(map[string]*paramValidate, 4)
	tpls := make(map[string]*tpl, 4)

GENERATOR:
	for _, node := range generator.Decls {
		switch node.(type) {
		case *ast.GenDecl:
			decl := node.(*ast.GenDecl)
			for _, spec := range decl.Specs {
				thisType, ok := spec.(*ast.TypeSpec)
				if !ok {
					continue
				}

				thisStruct, ok := thisType.Type.(*ast.StructType)
				if !ok {
					continue
				}

				paramName := thisType.Name.Name
				methodName := "validate" + paramName

				paramTpl := &paramValidate{
					Param:  paramName,
					Fields: make([]tplField, 0, len(thisStruct.Fields.List)),
				}

				for _, f := range thisStruct.Fields.List {
					if f.Tag == nil {
						continue GENERATOR
					}
					t := reflect.StructTag(f.Tag.Value[1 : len(f.Tag.Value)-1])
					tValue, ok := t.Lookup(AttrValidator)
					if !ok {
						continue GENERATOR
					}

					tplF := tplField{}
					tplF.Name = f.Names[0].Name
					tplF.Param = strings.ToLower(tplF.Name)
					fType := f.Type.(*ast.Ident).Name

					switch fType {
					case "int":
						tplF.Default = "0"
					case "string":
						tplF.Default = `""`
					default:
						log.Fatalln("unsupported", fType)
					}

					tValues := strings.Split(tValue, ",")

					for _, tVal := range tValues {
						if params := TagParamname.FindAllStringSubmatch(tVal, 1); len(params) > 0 {
							tplF.Param = params[0][1]
						}

						if TagRequired.MatchString(tVal) {
							tplF.Required = true
						}

						if max := TagMax.FindAllStringSubmatch(tVal, 1); len(max) > 0 {
							tplF.IsMax = true
							tplF.Max = max[0][1]
						}

						if min := TagMin.FindAllStringSubmatch(tVal, 1); len(min) > 0 {
							tplF.IsMin = true
							tplF.Min = min[0][1]
						}

						if defaults := TagDefault.FindAllStringSubmatch(tVal, 1); len(defaults) > 0 {
							tplF.IsDefault = true
							dValue := defaults[0][1]
							if fType == "string" {
								dValue = fmt.Sprintf(`"%s"`, dValue)
							}
							tplF.DefaultValue = dValue
						}
						if enum := TagEnum.FindAllStringSubmatch(tVal, 1); len(enum) > 0 {
							enums := strings.Split(enum[0][1], "|")
							tplF.EnumError = fmt.Sprintf(`%s must be one of [%s]`,
								tplF.Param, strings.Join(enums, ", "))
							if fType == "string" {
								for i := range enums {
									enums[i] = fmt.Sprintf(`"%s"`, enums[i])
								}
							}
							tplF.IsEnum = true
							tplF.EnumType = fType
							tplF.Enums = strings.Join(enums, ",")
						}
					}
					paramTpl.Fields = append(paramTpl.Fields, tplF)
				}

				if len(paramTpl.Fields) > 0 {
					paramTpls[methodName] = paramTpl
				}
			}

		case *ast.FuncDecl:
			decl := node.(*ast.FuncDecl)
			methodName := ""
			if decl.Recv != nil {
				for _, r := range decl.Recv.List {
					if expr, ok := r.Type.(*ast.StarExpr); ok {
						if ident, ok := expr.X.(*ast.Ident); ok {
							methodName = ident.Name
						}
					}
				}
			}

			if methodName != "" {
				if decl.Doc == nil {
					continue GENERATOR
				}
				res, err := getResource(decl)
				if err != nil {
					continue
				}

				if t, ok := tpls[methodName]; ok {
					t.Resources = append(t.Resources, *res)
				} else {
					tpls[methodName] = &tpl{
						ResourceName: methodName,
						Resources:    []resource{*res},
					}
				}
			}
		default:
			continue
		}
	}
	for _, out := range paramTpls {
		_ = tplValidation.Execute(result, out)
	}
	for _, out := range tpls {
		_ = tplHandler.Execute(result, out)
	}
}

func getResource(decl *ast.FuncDecl) (*resource, error) {
	for _, l := range decl.Doc.List {
		if len(l.Text) > 0 && strings.HasPrefix(l.Text, AttrApigen) {
			res := resource{}
			data := strings.Replace(l.Text, AttrApigen, "", -1)
			if err := json.Unmarshal([]byte(data), &res); err != nil {
				return nil, err
			}
			res.Func = decl.Name.Name
			for _, param := range decl.Type.Params.List {
				if ident, ok := param.Type.(*ast.Ident); ok {
					res.Param = ident.Name
				}
			}
			return &res, nil
		}
	}
	return nil, fmt.Errorf("no comment with %s", AttrApigen)
}
