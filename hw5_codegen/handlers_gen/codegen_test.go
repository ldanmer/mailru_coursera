package main

import (
	"github.com/prometheus/common/log"
	"os"
	"testing"
)

func TestHandlerTemplate(t *testing.T) {
	if err := tplHandler.Execute(os.Stdout, tpl{
		ResourceName: "MyApi",
		Resources: []resource{
			{
				Url:    "/user/profile",
				Auth:   false,
				Method: "",
				Func:   "Profile",
				Param:  "ProfileParams",
			},
			{
				Url:    "/user/create",
				Auth:   true,
				Method: "POST",
				Func:   "Create",
				Param:  "CreateParams",
			},
		},
	}); err != nil {
		log.Fatal(err)
	}
}

func TestValidateTemplate(t *testing.T) {
	if err := tplValidation.Execute(os.Stdout, paramValidate{
		Param: "",
		Fields: []tplField{{
			Name:         "test",
			Param:        "test_param",
			Default:      "0",
			Required:     true,
			IsEnum:       true,
			Enums:        "1,2",
			IsDefault:    true,
			DefaultValue: "default",
		}},
	}); err != nil {
		log.Fatal(err)
	}
}
